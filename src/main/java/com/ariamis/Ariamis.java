package com.ariamis;

import com.ariamis.proxy.CommonProxy;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

@Mod(modid = Ariamis.MODID, version = Ariamis.VERSION)
public class Ariamis {
    public static SimpleNetworkWrapper network;
    public static final String MODID = "ariamis";
    public static final String VERSION = "1.0";
    @SidedProxy(modId = MODID, clientSide = "com.ariamis.proxy.ClientProxy", serverSide = "com.ariamis.proxy.CommonProxy")
    public static CommonProxy proxy;

    @EventHandler
    public void _init(FMLPreInitializationEvent event) {
        proxy.preInit(event);
    }

    @EventHandler
    public void init(FMLInitializationEvent event) {
        Ariamis.network();
        proxy.init(event);
    }
    @EventHandler
    public void init_(FMLPostInitializationEvent event) {
        proxy.postInit(event);
    }

    public static void network() {
        Ariamis.network = NetworkRegistry.INSTANCE.newSimpleChannel(Ariamis.MODID);
    }

    public static CreativeTabs creativeTab = new CreativeTabs(MODID) {
        @Override
        public Item getTabIconItem() {
            return null;
        }

        @Override
        public ItemStack getIconItemStack() {
            return new ItemStack(Items.golden_carrot, 1, 1);
        }
    };
}
